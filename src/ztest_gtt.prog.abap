*&---------------------------------------------------------------------*
*& Report ztest_gtt
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT ztest_gtt.

CLASS controller DEFINITION.

  PUBLIC SECTION.
    METHODS: start.

ENDCLASS.

CLASS controller IMPLEMENTATION.

  METHOD start.



    TYPES: tt_t100 TYPE STANDARD TABLE OF t100
                        WITH NON-UNIQUE DEFAULT KEY.

    INSERT zt100_gtt FROM TABLE @(
               VALUE tt_t100(
                 ( sprsl = 'E' arbgb = '00' msgnr = '001' )
                 ( sprsl = 'E' arbgb = '00' msgnr = '002' ) ) ).

    SELECT FROM t100
           INNER JOIN zt100_gtt ON t100~sprsl = zt100_gtt~sprsl
                               AND t100~arbgb = zt100_gtt~arbgb
                               AND t100~msgnr = zt100_gtt~msgnr
           FIELDS t100~*
           INTO TABLE @DATA(t100_tab).

    SELECT FROM zt100_gtt
           FIELDS *
           INTO TABLE @DATA(text).

    DELETE FROM zt100_gtt.

    cl_demo_output=>display( text ).
    cl_demo_output=>display( t100_tab ).

  ENDMETHOD.

ENDCLASS.

START-OF-SELECTION.
  NEW controller( )->start( ).
